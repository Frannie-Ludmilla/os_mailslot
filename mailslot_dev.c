
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/module.h>
#include <linux/device.h>         // Header to support the kernel Driver Model
#include <linux/kernel.h>         // Contains types, macros, functions for the kernel
#include <linux/slab.h>
#include <asm/uaccess.h>          // Required for the copy to user function
#include <linux/wait.h>
#include <linux/sched.h>            //Pid Informations
//#include <asm/atomic.h>              //for ticket lock support
//#include <linux/spinlock.h>
//...........HEADERS FOR KFIFO...................
#include <linux/types.h>
#include <linux/mutex.h>
#include <linux/kfifo.h>
#include <linux/proc_fs.h>
#include "mailslot_dev.h"

#define  DEVICE_NAME "ms"    ///< The device will appear at /dev/ms$ using this value
#define  CLASS_NAME  "ms"        ///< The device class -- this is a character device driver
//SMALL SIZE
#define SIZE_MSG 2048 //Initial MAX recsize of fifo_msg
//#define FIFO_SIZE 128
//BIG SIZE 
//#define SIZE_MSG 4096
#define FIFO_SIZE 131072
#define MIN_MSG_SIZE 64
#define MAX_MSG_SIZE 4096

#define OK 0
#define ERR -1

#define FALSE 0
#define TRUE 1
//.............IOCTL.............................
/* Use 'DE' = 222  as magic number */
#define MS_IOC_MAGIC  'DE'
/* We must use a 8-bit number in our code 0-255 that does not collide with the 
 * ones in Documentation/ioctl/ioctl_numbers.txt */
#define MS_IOCRESET   _IO(MS_IOC_MAGIC, 0) 
#define MAILSLOT_IOCTSIZE _IOW(MS_IOC_MAGIC,   1,int)
#define MAILSLOT_IOCTL_BLOCKING _IOW(MS_IOC_MAGIC,   2,int)
#define MS_IOC_MAXNR 2
#define TYPE(minor)	(((minor) >> 4) & 0xf)	/* high nibble */
#define NUM(minor)	((minor) & 0xf)		/* low  nibble */

#define DEBUG
//Getting rid of printk if not debug mode

// The prototype functions for the character driver -- must come before the struct definition
static int mailslot_open(struct inode *, struct file *);
//static int mailslot_release(struct inode *, struct file *);
static ssize_t mailslot_read(struct file *, char *, size_t, loff_t *);
static ssize_t mailslot_write(struct file *, const char *, size_t, loff_t *);
static long mailslot_ioctl(struct file *, unsigned int, unsigned long);
static void clean_mailslot_devices(int end_index);

int mailslot_major = MAILSLOT_MAJOR;
int mailslot_devs = MAILSLOT_DEVS;
struct mailslot_dev* mailslot_devices;
int error_in_allocating_kfifo_dev;
char name_dev_index[8];                
static struct class* mscharClass = NULL; ///< The device-driver class struct pointer

//In this structure we specify the file operations that can be performed wrt /dev/mailslot
static const struct file_operations mailslot_fops = {
    .owner = THIS_MODULE,
    .read = mailslot_read,
    .write = mailslot_write,
    .open = mailslot_open,
    .unlocked_ioctl = mailslot_ioctl,
};

//OPEN for MAILSLOT

int mailslot_open(struct inode* inode, struct file* filep) {
    struct mailslot_dev *dev;
	int errFifo;	
    dev = container_of(inode->i_cdev, struct mailslot_dev, cdev);


	if(dev->fifo_initialized==FALSE){
		//Try to initialize the kfifo...
    	dev->size = FIFO_SIZE;
    	errFifo = kfifo_alloc(&dev->fifo_msg, FIFO_SIZE, GFP_KERNEL);
    	if (errFifo) {
        	printk(KERN_ALERT "Error %d while adding a kfifo to mailslot_dev instance %d at line%d", errFifo, dev->number, __LINE__);
        	return errFifo; //Why? An instance without a kfifo is useless. 
    	}

	dev->fifo_initialized=TRUE;

    printk("Size kfifo %d", kfifo_size(&dev->fifo_msg));
	}

	filep->private_data = dev;

    return 0;
}

//READ for MAILSLOT [ old signature: static ssize_t mailslot_read(struct file *filep, char *buffer, size_t len, loff_t *offset) { ]

static ssize_t mailslot_read(struct file * filep, char *buffer, size_t len, loff_t* offset) {
    unsigned int copied = 0;
    int ret;
    int length_msg;

    struct mailslot_dev* current_dev = filep->private_data;
    struct kfifo_rec_ptr_2* current_kfifo = &current_dev->fifo_msg;
    //KFIFO: Note that with only one concurrent reader and one concurrent
    //writer, you don't need extra locking to use these macro.
    struct mutex* current_read_lock = &current_dev->read_lock;
    wait_queue_head_t *current_read_queue = &current_dev->ms_read_queue;
    wait_queue_head_t *current_write_queue = &current_dev->ms_write_queue;

    //Try to achieve the lock, else return an errno prompting to try again
    if (mutex_lock_interruptible(current_read_lock)) {
        return -ERESTARTSYS;
    }

    while (kfifo_is_empty(current_kfifo)) {
        mutex_unlock(current_read_lock);
        if (filep->f_flags & O_NONBLOCK) {
            return -EAGAIN;
        }
        if (wait_event_interruptible(*current_read_queue, !kfifo_is_empty(current_kfifo)))
        //if (wait_event_interruptible(*current_read_queue, 1))
            return -ERESTARTSYS;
        if (mutex_lock_interruptible(current_read_lock))
            return -ERESTARTSYS;
    }

    //See how many bytes we must transfer [sizeof(next_element)]...
    length_msg = kfifo_peek_len(current_kfifo);
    //...and check if the user buffer can accomodate them
    if (len < length_msg) {
        mutex_unlock(current_read_lock);
        return -ENOMEM;
    }

    ret = kfifo_to_user(current_kfifo, buffer, length_msg, &copied);

    if (ret == 0) {
        if (copied == length_msg) {
            //Case 1: everything went ok
            printk(KERN_INFO "MS_DEV: Record size: %d , # transmitted bytes %d from user tid %d to instance %d \n",
                    length_msg, copied, (int) task_pid_nr(current), current_dev->number);
        } else { //Case 2: Short read
            printk(KERN_ALERT "MS_DEV: SHORT READ: Record size: %d , # transmitted bytes %d from user tid %d to instance %d \n",
                    length_msg, copied, (int) task_pid_nr(current), current_dev->number);
            ret = -EFAULT;
        }
    } else { //Case 3: ret is not zero, an error must have occurred.
        printk(KERN_ALERT "MS_DEV: ERROR : kfifo_to_user returned %d from instance %d, lenght_msg %d, copied  %d, user tid %d\n",
                ret, current_dev->number, length_msg, copied, (int) task_pid_nr(current));
    }

    //Whatever case I had, I must unlock the read_lock
    mutex_unlock(current_read_lock);
    wake_up_interruptible(current_write_queue);
    return ret ? ret : copied;

}

//WRITE for MAILSLOT

static ssize_t mailslot_write(struct file* filep, const char *buffer, size_t len, loff_t *offset) {
    int ret;
    unsigned int copied = 0;
    int filled_fifo;
    int free_fifo;

    struct mailslot_dev* current_dev = filep->private_data;
    struct kfifo_rec_ptr_2* current_kfifo = &current_dev->fifo_msg;
    struct mutex* current_write_lock = &current_dev->write_lock;
    wait_queue_head_t *current_read_queue = &current_dev->ms_read_queue;
    wait_queue_head_t *current_write_queue = &current_dev->ms_write_queue;

    if (mutex_lock_interruptible(current_write_lock))
        return -ERESTARTSYS;
    
    //Message too big: abort before taking unnecessarly the lock
    //i have the mutex, max_msg_size is stable
    if (len > current_dev->max_msg_size) {
        printk(KERN_ALERT "MS_DEV: cannot store msg with size %d because MAX_SIZE is %d and the available space is %d (bytes) \n", len, current_dev->max_msg_size, free_fifo);
        mutex_unlock(current_write_lock);
        return -ENOMEM;
    }

    filled_fifo = kfifo_len(current_kfifo);
    free_fifo = current_dev->size - filled_fifo;

    //Check if we have enough room to accomodate the message from the user
    while (len > free_fifo) {
        mutex_unlock(current_write_lock);
        if (filep->f_flags & O_NONBLOCK) {
            return -EAGAIN;
        }
        if (wait_event_interruptible(*current_write_queue, current_dev->size - kfifo_len(current_kfifo) > len))
            return -ERESTARTSYS;
        if (mutex_lock_interruptible(current_write_lock))
            return -ERESTARTSYS;
        //meanwhile, max_msg_size is changed
        if (len > current_dev->max_msg_size) {
            printk(KERN_ALERT "MS_DEV: cannot store msg with size %d because MAX_SIZE is %d and the available space is %d (bytes) \n", len, current_dev->max_msg_size, free_fifo);
            mutex_unlock(current_write_lock);
            return -ENOMEM;            
        }
        filled_fifo = kfifo_len(current_kfifo);
        free_fifo = current_dev->size - filled_fifo;       
    }

#ifdef DEBUG
    printk(KERN_INFO "Mailslot @%d: write fifo size %d - occupied %d = %d free \n", __LINE__, current_dev->size, filled_fifo, free_fifo);
#endif

    //If we are here we can write to the selected kfifo

    ret = kfifo_from_user(current_kfifo, buffer, (int) len, &copied);

    if (ret != 0 || copied == 0 || copied != len) { //Something wicked this way comes
        printk(KERN_ALERT "MS_DEV: ERROR : kfifo_from_user returned %d from instance %d, lenght_msg %d, copied  %d, user tid %d\n",
                ret, current_dev->number, len, copied, (int) task_pid_nr(current));
        ret = -EFAULT;
    } else {
        printk(KERN_INFO "MS_DEV: write: Instance %d received %d characters from the user %d\n",
                current_dev->number, copied, (int) task_pid_nr(current));
    }

    //Whatever case I had, I must unlock the read_lock
    mutex_unlock(current_write_lock);
    wake_up_interruptible(current_read_queue);
    return ret ? ret : copied;
}


//IOCTL for MAILSLOT

long mailslot_ioctl(struct file *filep, unsigned int cmd, unsigned long arg) {
    int err = 0;
    int tmp_val;

    struct mailslot_dev* current_dev = filep->private_data;
    struct mutex* current_write_lock = &current_dev->write_lock;

    printk(KERN_INFO "MS_DEV: ioctl call for instance %d with command %d\n", current_dev->number, cmd);

    // extract the type and number bitfields to check the cmds: return ENOTTY (inappropriate ioctl)

    if (_IOC_NR(cmd) > MS_IOC_MAXNR) return -ENOTTY;

    //The direction is a bitmask, and VERIFY_WRITE catches R/W transfers. `Type' is user-oriented, while
    //access_ok is kernel-oriented, so the concept of "read" and "write" is reversed

    if (_IOC_DIR(cmd) & _IOC_READ)
        err = !access_ok(VERIFY_WRITE, (void __user *) arg, _IOC_SIZE(cmd));
    else if (_IOC_DIR(cmd) & _IOC_WRITE)
        err = !access_ok(VERIFY_READ, (void __user *) arg, _IOC_SIZE(cmd));
    if (err) return -EFAULT;

    switch (cmd) {
        /* The following f() changes the message size for kfifo.*/
        case MAILSLOT_IOCTSIZE:
            if (!capable(CAP_SYS_ADMIN)) {
                printk(KERN_INFO "Mailslot:ERR: MAILSLOT_IOCTSIZE  not from root\n");
                return -EPERM;
            }
            if (__get_user(tmp_val, (int __user *) arg) != 0) {
                printk(KERN_INFO "Mailslot:ERR: MAILSLOT_IOCTSIZE error getting user parameter\n");
                return -EFAULT;
            }
            
            if (!((MIN_MSG_SIZE <= tmp_val) && (tmp_val < MAX_MSG_SIZE))) {
                printk(KERN_INFO "Mailslot: MAILSLOT_IOCTSIZE invalid msg size %d\n", tmp_val);
                return -EINVAL;
            }
            else {
				//Why do we check the write_lock? We do not want to change the maximum message size while someone is writing
                if (mutex_lock_interruptible(current_write_lock))
                    return -ERESTARTSYS;
                current_dev->max_msg_size = tmp_val;
#ifdef DEBUG
                printk(KERN_INFO "Mailslot: MAILSLOT_IOCTSIZE changed max size with %d\n", tmp_val);
#endif
                 mutex_unlock(current_write_lock);
                return OK;
            }
        //Change Session's Blocking Flag
        case MAILSLOT_IOCTL_BLOCKING:

            if (__get_user(tmp_val, (int __user *) arg) != 0) {
                printk(KERN_INFO "Mailslot:ERR: MAILSLOT_IOCTL_BLOCKING error getting user parameter\n");
                return -EFAULT;
            }
            //check user argument
            if (!((tmp_val==0)||(tmp_val==1))) {
                printk(KERN_INFO "Mailslot:ERR: MAILSLOT_IOCTL_BLOCKING invalid parameter\n");
                return -EINVAL;
            }

            //printk(KERN_INFO "tmp_val %d\n",tmp_val);
            //Taken from: http://lxr.free-electrons.com/source/fs/ioctl.c#L473

            unsigned int flag = O_NONBLOCK;

            //Update file flag
            spin_lock(&filep->f_lock);
            if (tmp_val)
                filep->f_flags |= flag;
            else
                filep->f_flags &= ~flag;
            spin_unlock(&filep->f_lock);

            return OK;

        default: /* Unreachable as cmd should be < MAXNR */
            printk(KERN_INFO "Mailslot:ERR: reached default as cmd is %d (macro %d) and arg is %d", cmd, MAILSLOT_IOCTSIZE, tmp_val);
            return -ENOTTY;
    }

    return 0;
}

//We have 256 instances of the device, each has a mailslot_dev struct. We have to initialize for each one its struct.
//We have to ask for a kfifo for each dev and to add their entry to sysfs.

static int mailslot_setup_cdev(struct mailslot_dev *dev, int index) {
    int err, devno = MKDEV(mailslot_major, index);

    cdev_init(&dev->cdev, &mailslot_fops); //Now, we have to assign for each device the operations that it can perform
    dev->cdev.owner = THIS_MODULE;
    dev->cdev.ops = &mailslot_fops;
    dev->max_msg_size = SIZE_MSG;

    mutex_init(&dev->read_lock);
    mutex_init(&dev->write_lock);

	dev->fifo_initialized=FALSE;
	
    //Try to add the new char device instance %index
    err = cdev_add(&dev->cdev, devno, 1);
    if (err) {
        printk(KERN_ALERT "Error %d adding mailslot_dev instance %d at line%d", err, index, __LINE__);
        return err; //Why? An instance that cannot be accessed to users as a device is useless.
    }

    //Let us register the device driver so it appears as /dev/ms$
    memset(name_dev_index, 0, sizeof (name_dev_index));
    //pointer to buffer, max bytes to write
    snprintf(name_dev_index, 7, "ms%d", index);

    dev->mscharDevice = device_create(mscharClass, NULL, MKDEV(mailslot_major, index), NULL, name_dev_index);
    if (IS_ERR(dev->mscharDevice)) { // Clean up if there is an error
        printk(KERN_ALERT "Failed to create the device\n");
        return PTR_ERR(dev->mscharDevice);
    }
    printk(KERN_INFO "Mailslot: %d device class created correctly\n", index); // Made it! device was initialized

    //If everything worked out fine return OK=0
    return 0;
}


//Set how many devices we have to clean (all the 256 instances or less if setup function failed)

static void clean_mailslot_devices(int end_index) {
    int i;
    for (i = 0; i < end_index; i++) {
        //delete the device
        cdev_del(&mailslot_devices[i].cdev);
        //free the kfifo only if it has been initialized
		if(mailslot_devices[i].fifo_initialized==TRUE)
			kfifo_free(&mailslot_devices[i].fifo_msg);
        device_destroy(mscharClass, MKDEV(mailslot_major, i)); // remove the device
    }
}

static void mailslot_exit(void) {
    int last_dev_to_clean;
    
    printk(KERN_INFO "MS_DEV: %d: Performing the cleanup for the Mailslot module...\n", __LINE__);
	
	//Launch clean_mailslot_devices
    if (error_in_allocating_kfifo_dev) {
        last_dev_to_clean = error_in_allocating_kfifo_dev;
    } else
        last_dev_to_clean = mailslot_devs;

    clean_mailslot_devices(last_dev_to_clean);

    //Clear the allocated memory to store the devices structs
    kfree(mailslot_devices);
    //De-register the device class
    class_unregister(mscharClass);
    class_destroy(mscharClass);
    //De-register the region
    unregister_chrdev_region(MKDEV(mailslot_major, 0), mailslot_devs);

    printk(KERN_INFO "MS_DEV: %d: Cleanup DONE. Goodbye! \n", __LINE__);

}


//This function tells what the kernel must perform when loading the module

int mailslot_init(void) {
    int result, i;
    int error_setup;
	dev_t root_dev;
    error_in_allocating_kfifo_dev = 0;

    root_dev = MKDEV(mailslot_major, 0);

    //If we were assigned directly a dynamic major number by LANA, we would have set it in the macro MAILSLOT_MAJOR
	//since we do not have it we set it to zero prompting the module to acquire a major number (else statement) 
    if (mailslot_major) {
        result = register_chrdev_region(root_dev, mailslot_devs, DEVICE_NAME);
    } else { //Try again to acquire a major number
        //int alloc_chrdev_region (	dev_t * dev, unsigned baseminor [first of the requested range of minor numbers ],
        //unsigned count, const char *	name);
        result = alloc_chrdev_region(&root_dev, 0, mailslot_devs, DEVICE_NAME);
        mailslot_major = MAJOR(root_dev);
    }

    if (result < 0)
        return result;

    printk(KERN_INFO "MS_DEV: %d: Initializing the Mailslot module...\n", __LINE__);

    //kmalloc (#devices) x (size of (struct mailslot_dev))
    mailslot_devices = kmalloc(mailslot_devs * sizeof (struct mailslot_dev), GFP_KERNEL);
    if (!mailslot_devices) {
        result = -ENOMEM;
        goto cleanup; //unregister the device and return the errno
    }

    //Reset to 0 all the allocated mem (no junk numbers, pointers etc..)
    memset(mailslot_devices, 0, mailslot_devs * sizeof (struct mailslot_dev));


    //Create the device class
    mscharClass = class_create(THIS_MODULE, CLASS_NAME);
    if (IS_ERR(mscharClass)) { // Check for error and clean up if there is
        unregister_chrdev(mailslot_major, DEVICE_NAME);
        printk(KERN_ALERT "Failed to register device class\n");
        return PTR_ERR(mscharClass); // Correct way to return an error on a pointer
    }
    printk(KERN_INFO "MS_DEV: device class registered correctly\n");

    //Instantiate all the devices' structs...then in the setup we will add them to sys. 
    for (i = 0; i < mailslot_devs; i++) {
        mailslot_devices[i].number = i; //Each struct knows which number it is (DEBUGGING Purpose)
        init_waitqueue_head(&mailslot_devices[i].ms_read_queue);
        init_waitqueue_head(&mailslot_devices[i].ms_write_queue);
        error_setup = mailslot_setup_cdev(mailslot_devices + i, i); //trigger for each struct the setup function	
        if (error_setup) { //equal to error_setup!=0
            error_in_allocating_kfifo_dev = i;
            break;
        }
    }

    if (error_setup)
        mailslot_exit(); //Start immediately the cleaning up(exit function of module). All the instances must work or none.

    printk(KERN_INFO "MS_DEV: %d: ...module initialization successful! \n", __LINE__);
    return 0;

cleanup:
    unregister_chrdev_region(root_dev, mailslot_devs); // ( dev_t, #devices= range allocated)
    printk(KERN_ALERT "MS_DEV: %d: ...module initialization FAILED \n", __LINE__);
    return result;
}


module_init(mailslot_init);
module_exit(mailslot_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Giovanni Farina & Francesca Pesci>");
MODULE_DESCRIPTION("\"Mailslot\" minimal module");
MODULE_VERSION("dev");
