#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<sys/ioctl.h>

#define BUFFER_LENGTH 1024               ///< The buffer length (crude but fine)
#define MS_IOC_MAGIC  'DE'
#define MAILSLOT_IOCTSIZE _IOW(MS_IOC_MAGIC,   1,int)
#define MAILSLOT_IOCTL_BLOCKING _IOW(MS_IOC_MAGIC,   2,int)

static char receive[BUFFER_LENGTH];     ///< The receive buffer from the LKM

int main() {
    int ret, fd;
    char stringToSend[BUFFER_LENGTH];
    fd = open("/dev/ms0", O_RDWR); // Open the device with read/write access
    if (fd < 0) {
        perror("Failed to open the device...");
        return errno;
    }
    while (1) {
        printf("Type in a short string to send to the kernel module:\n");
        scanf("%[^\n]%*c", stringToSend);
        ret = write(fd, stringToSend, strlen(stringToSend)); // Send the string to the LKM
        if (ret < 0) {
            //perror("Failed to write the message to the device.");
            printf("Failed to write the message to the device, errno: %d\n", errno);
            return errno;
        }
    }

}

