#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<fcntl.h>
#include<string.h>
#include<sys/ioctl.h>
#include <sys/types.h>

#define BUFFER_LENGTH 1024               ///<The buffer length (crude but fine)
#define MS_IOC_MAGIC  'DE'
#define MAILSLOT_IOCTSIZE _IOW(MS_IOC_MAGIC,   1,int)
#define MAILSLOT_IOCTL_BLOCKING _IOW(MS_IOC_MAGIC,   2,int)

static char receive[BUFFER_LENGTH]; ///< The receive buffer from the LKM

static int fd;

int main() {
    int ret, dev;
    char buffer[BUFFER_LENGTH];
    pid_t proc_pid = getpid();

    printf("Type the number of mailslot that you want to access\n");

    do {
        ret = scanf("%d", &dev);
        if (ret == 0) {
            scanf("%*[^\n]");
            printf("a not valid input is inserted. Try again...\n");
        } else
            if (dev < 0 || dev > 255) {
            printf("a not valid input is inserted. Try again...\n");
            ret = 0;
        }

    } while (ret == 0);

    sprintf(buffer, "/dev/ms%d", dev);

    fd = open(buffer, O_RDWR); // Open the device with read/write access
    if (fd < 0) {
        perror("Failed to open the device...");
        return errno;
    } else
        printf("Mailslot successfully opened\n\n");

	//WARNING: NO PARAMETERS CHECK
    while (1) {
        scanf("%*[^\n]");
        printf("What operation do you want to execute?\n\tread: 'read numberOfReads'\n\twrite: 'write numberOfWrites'\n\tioctl changeMsg: 'ioctl 8<sizeOfMsg<4096'\n\tioctl NO-blocking: 'ioctl 0|1'\n\texit: exit 0\n");
        int j;
        scanf("%s %d", buffer, &j);
        scanf("%*[^\n]");
        
        ret = strcmp(buffer, "read");
        if (ret == 0) {
            int i;
            for (i = 0; i < j; i++) {
                memset(buffer, 0, sizeof (buffer));
                ret = read(fd, buffer, BUFFER_LENGTH); // Read the response from the LKM
                if (ret < 0) {
                    //perror("Failed to read the message from the device.");
                    printf("Failed to read the message from the device, errno: %d\n", errno);
                    //return errno;
                }
                else
			printf("read string: %s\n", buffer);
            }
            continue;
        }
        
        ret = strcmp(buffer, "write");
        if (ret == 0) {
            int i;
            for (i = 0; i < j; i++) {
                sprintf(buffer, "Process %d, Iteration %d", proc_pid, i);
                ret = write(fd, buffer, strlen(buffer)); // Send the string to the LKM
                if (ret < 0) {
                    //perror("Failed to write the message to the device.");
                    printf("Failed to write the message to the device, errno: %d\n", errno);
                    //return errno;
                }
            }
            continue;
        }
        
        ret = strcmp(buffer, "ioctl");
        if (ret == 0) {
            if (j > 7 && j < 4097) {
                ret = ioctl(fd, MAILSLOT_IOCTSIZE, &j);
                if (ret != 0)
                    printf("Ioctl MAILSLOT_IOCTSIZE with error code %d\n", ret);
            } else {
                if (j == 0 || j == 1) {
                	ret = ioctl(fd, MAILSLOT_IOCTL_BLOCKING, &j);
                	if (ret != 0) {
                   		printf("Ioctl MAILSLOT_IOCTL_BLOCKING with error code %d\n", ret);
                	}
            	} else
                	printf("Invalid ioct value\n");
	    }
            continue;
        }
        
        ret = strcmp(buffer, "exit");
        if (ret == 0) {
            close(fd);
	    return 0;
        }
        
        printf("Invalid Command\n");
        
    }
}
