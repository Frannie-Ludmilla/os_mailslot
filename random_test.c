#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#define NTHREAD 12
#define TIMER_ALARM 50
#define PRNG_BUFSZ 8
#define BUFFER_LENGTH 2048               ///< The buffer length (crude but fine)
//char buffer[BUFFER_LENGTH];     ///< The receive buffer from the LKM

long fsize;
const int n_Dev=256;
char* buffer;
const int n_try=40;


void *threadWork(void *arg){
	//pthread_id_np_t   tid;
	pthread_t tid;
   	tid= pthread_self();
	int random_time;
	char buffer_read[BUFFER_LENGTH];
	char name_dev[12];
	int i=0;
	for (i; i<n_try; i++){
		memset(name_dev,0,12);
		int rand_dev_w;
		random_r((struct random_data*)arg, &rand_dev_w);
		rand_dev_w= rand_dev_w % 256;
		snprintf(name_dev, 12, "/dev/ms%d", rand_dev_w);

		//First cycle: write all the 1,5KB of msg to each dev
		int fd = open(name_dev, O_RDWR);             // Open the device with read/write access
   		if (fd < 0){
     		printf("Failed to open the device %s\n",name_dev);
      		//return errno;
   		}
		else{
		
			if (write(fd, buffer, strlen(buffer)) < 0) {
        	    if (errno == EBUSY) {
        	        printf("Failed to write, mailslot %s in use\n",name_dev);
        	    } else {
        	        if (errno == ENOMEM) {
        	            printf("Failed to write, FULL mailslot /dev/ms%d\n",i);
       	         } else
        	            printf("Failed to write, error code %d\n", errno);
       	     }
			}
			close(fd);
		}
		random_r((struct random_data*)arg, &random_time);
		random_time= random_time%4; //I want an interval of [0,1,2,3] sec in which the process sleep

		printf("Tid %lu completed the write from %s ...sleep for %d secs\n",tid,name_dev,random_time);
		sleep(random_time);
		printf("Tid %lu awoke from sleep to read...\n",tid);
		
		//Second cycle: read from a random device

		int rand_dev;
		random_r((struct random_data*)arg, &rand_dev);
		rand_dev= rand_dev%256;

		memset(name_dev,0,12);
		snprintf(name_dev, 12, "/dev/ms%d", rand_dev);

		fd = open(name_dev, O_RDWR);             // Open the device with read/write access
   		if (fd < 0){
     		printf("Failed to open the device %s\n",name_dev);
      		//return errno;
   		}
		else{
			memset(buffer_read, 0 ,BUFFER_LENGTH);
        
			if (read(fd, buffer_read, BUFFER_LENGTH) < 0) {
            	if (errno == EBUSY) {
                	printf("Failed to read, from mailslot %d in use\n", rand_dev);
          	  } else {
            	    if (errno == ENOMEM) {
                	    printf("Failed to read, NO messages\n");
               	 } else
                	    printf("Failed to read, error code %d\n", errno);
            	}
       	 }else
				printf("Tid %lu: %d chars read\n",tid,strlen(buffer_read));
		close(fd);
		}
	}

}

void timer_handler() {
 	int i=0;
	char name_dev[12];
	printf("ALARM: Rewriting things to fill up my kfifoes\n");
	for (i; i<n_Dev; i++){
		memset(name_dev,0,12);
		snprintf(name_dev, 12, "/dev/ms%d", i);
		int dev_fd = open(name_dev, O_RDWR);             // Open the device with read/write access
   		if (dev_fd < 0){
     		printf("Failed to open the device %s\n",name_dev);
      		return errno;
   		}
		int index;
		for(index=0; index<3; index++){
			if (write(dev_fd, buffer, strlen(buffer)) < 0) {
           		if (errno == EBUSY) {
           	    	printf("Failed to write, mailslot %s in use\n",name_dev);
				} else {
					if (errno == ENOMEM) {
						printf("Failed to write, FULL mailslot /dev/ms%d\n",i);
					}else
						printf("Failed to write, error code %d\n", errno);
           		 }
			}
		}
		close(dev_fd);
	}
	printf("ALARM: ..done!\n");
	alarm(TIMER_ALARM);
}


int main(){
	FILE* fp= fopen("msg.txt", "r");
	if(fp!=NULL){
		fseek(fp, 0, SEEK_END);	
		fsize=ftell(fp);
		fseek(fp, 0, SEEK_SET);

		buffer= (char*)malloc(fsize+1);
		fread(buffer,fsize, 1,fp);
		printf("%s",buffer);

		fclose(fp);

		pthread_t tids[NTHREAD];
		int i=0;
		
		printf("Writing Mrs Dalloway to all the devices!\n");

		//srand(time(NULL));
		
		char name_dev[12];
		for (i; i<n_Dev; i++){
			memset(name_dev,0,12);
			snprintf(name_dev, 12, "/dev/ms%d", i);
			int dev_fd = open(name_dev, O_RDWR);             // Open the device with read/write access
   			if (dev_fd < 0){
     			printf("Failed to open the device %s\n",name_dev);
      			return errno;
   			}
		
			if (write(dev_fd, buffer, strlen(buffer)) < 0) {
            	if (errno == EBUSY) {
            	    printf("Failed to write, mailslot %s in use\n",name_dev);
          	  } else {
           	     if (errno == ENOMEM) {
           	         printf("Failed to write, FULL mailslot /dev/ms%d\n",i);
           	     } else
            	        printf("Failed to write, error code %d\n", errno);
           	 }
			}

		close(dev_fd);
		}

		printf("Flowers bought!\n");

		struct random_data* rand_states = (struct random_data*)calloc(NTHREAD * 64, sizeof(struct random_data));
		char* rand_statebufs = (char*)calloc(NTHREAD*64, PRNG_BUFSZ);

		alarm(TIMER_ALARM);
		signal(SIGALRM, timer_handler);

		printf("Starting threads...\n");
		for (i=0; i<NTHREAD; i++) {
			initstate_r(random(), &rand_statebufs[i*64], PRNG_BUFSZ, &rand_states[i*64]);
			if (pthread_create(&tids[i], NULL, &threadWork, &rand_states[i*64])) {
                	printf("cannot create thread for error %d\n", i);
            	}
        }

		printf("...joining threads...\n");


		for (i=0; i<NTHREAD; i++) {
      		if (pthread_join(tids[i],NULL))
			   	printf("cannot joint thread for error %d\n", i);
		}


		printf("...DONE!\n");
		free(buffer);
		free(rand_states);
		free(rand_statebufs);

	}
	else
		printf("cannot store msg to buffer\n");

	return 0;
}
