# README #
Linux Kernel Module based on the specification at [http://www.dis.uniroma1.it/~quaglia/DIDATTICA/AOSV/examination.html](http://www.dis.uniroma1.it/~quaglia/DIDATTICA/AOSV/examination.html).

To check the code:

**make** //compiles everything

**make link** //performs insmod

**make remove** //removes the binaries and performs rmmod

Tests:
Using text_exec is possible to
	- make an arbitrary number of consequency write
	- make an arbitrary number of consequency read
		-> in order to see blocking mechanism
	- test ioctl options
	- changing the maximum size of a single mailslot message, with super user privilage

text_write.c and test_read.c

using test_write and test_read is possible to verify the checks on message size, the data separation and the atomic delivery
(changing hard-coded size of buffers)

test_rand will employ threads to read/write concurrently to random mailslots: it starts by reading the message stored in msg.txt

Developers:
Giovanni Farina
Francesca Pesci