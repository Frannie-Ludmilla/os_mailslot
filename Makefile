obj-m := mailslot_dev.o 

KDIR  := /lib/modules/$(shell uname -r)/build
PWD   := $(shell pwd)
RULES := /etc/udev/rules.d/
CC := gcc

default:
	$(MAKE) -C $(KDIR) M=$(PWD) modules
	$(CC) -w test.c -o test_exec
	$(CC) -w test_read.c -o test_read
	$(CC) -w test_write.c -o test_write
	$(CC) -g -pthread random_test.c -o test_rand


link:
	sudo cp $(PWD)/mailslot.rules $(RULES)99-mailslot.rules
	sudo insmod $(PWD)/mailslot_dev.ko
	
remove:
	sudo rmmod mailslot_dev
	sudo rm $(RULES)99-mailslot.rules	
	make clean

clean:
	$(MAKE) -C $(KDIR) M=$(PWD) clean
	-rm test_exec
	-rm test_read
	-rm test_write
	-rm test_rand
