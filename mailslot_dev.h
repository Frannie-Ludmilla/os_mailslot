#include <linux/ioctl.h>
#include <linux/cdev.h>

//0 is the code for asking a dynamic major
#define MAILSLOT_MAJOR 0 
#define MAILSLOT_DEVS 256

struct mailslot_dev {
    //struct mailslot_dev *next;  /* next listitem */

    //Inherited stuff
    //int order;                /* the current allocation order */
    //int qset;                 /* the current array size */
    int number;
	int fifo_initialized;
    //Stuff related to kfifo
    struct kfifo_rec_ptr_2 fifo_msg;
    size_t size; //Size of kfifo
    size_t max_msg_size; //Max size for msgs
    //Stuff for blocking
    struct mutex read_lock;
    struct mutex write_lock;
    struct device* mscharDevice;
    //We are dealing with a cdev: all the mailslot ops will be set in cdev
    struct cdev cdev;
    //wait queues when the mailslot is empty or full
    wait_queue_head_t ms_write_queue, ms_read_queue;
};

